var unirest = require('unirest');
var request = require('request');
var cheerio = require('cheerio');
/******* To send support report and resolve comment to chat application *******/
module.exports = {
	supportnotes: function(req, res){
	return new Promise(function(resolve, reject) {
        getAccessCode(req, res)
            .then(function (response) {
                data = JSON.parse(response);
                if(data && data.access_token){
                    var comment = cheerio.load(req.body.cliqConfig, {normalizeWhitespace: true, recognizeSelfClosing: true, xmlMode: false});
                    var supportComment = "";
                    var chennal = "level-2"
                    if(req.body.stageName && req.body.stageName == "Support"){
                        supportComment = req.body.doi+'|'+ comment('.category').text() + " | " + comment('.priority').text() + " | " + comment('.stage').text() + " | " + comment('.level').text() + " | " + comment('.issue-notes').text() + " - Moved to Support";
                        if(comment('.level').text() == "level-3"){
                            chennal = comment('variable[type="report"][level="level-3"]').attr('chennal')
                        }else{
                            chennal = comment('variable[type="report"][level="level-2"]').attr('chennal')
                        }
                    }else{
                        supportComment = '@'+req.body.supportComment.fullname + ' - '+  req.body.doi+'|'+ comment('.category').text() + " | " + comment('.sub-category').text() + " | " + comment('.type').text() +" | " + comment('.issue-notes').text() + " - Support resolved";
                        chennal = comment('variable[type="resolve"]').attr('chennal')
                    }
                    url = cms.config.resourceManager.cliqMSG;
                    url = url.replace('{chennelName}', chennal);
                    unirest.post(url)
                        .type('json')
                        .header('Authorization', 'Zoho-oauthtoken '+data.access_token)
                        .send({"text":supportComment})
                        .end(function (response) {
                        resolve(response);
                    });
                }else{
                    reject({
                        status: {
                            code: '400',
                            message: 'Access code missing'
                        },
                    });
                }
            })
            .catch(function (e) {
                reject(e);
            })
        })
	}
}

//To Get access token from zoho api
function getAccessCode(req, res) {
	return new Promise(function(resolve, reject) {
        var url = 'https://accounts.zoho.com/oauth/v2/token';
        var options = {
            method: 'POST',
            url,
            form: {
                grant_type: 'refresh_token',
                client_id: cms.config.zohocliq.client_id,
                client_secret: cms.config.zohocliq.client_secret,
                refresh_token: cms.config.zohocliq.refresh_token
            }
        };

        request(options, (error, response, body) => {
            if (error) {
               reject(error);
            } else {
                resolve(body);
            }
        });
    })
}
